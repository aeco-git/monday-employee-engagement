import React, { useState } from "react";
import { CardData } from "../types";

import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Link from "@material-ui/core/Link";
import SvgIcon, { SvgIconProps } from "@material-ui/core/SvgIcon";
import CircularProgress from "@material-ui/core/CircularProgress";

const opacity = 1;
const useStyles = makeStyles({
  root: {
    height: "100%",
    borderRadius: 16,
    boxShadow: "0.0px 6.0px 20.0px 0px rgba(0, 0, 0, 0.2)",
    display: "block",
    position: "relative",
  },
  overLay: {
    background: `linear-gradient(150deg, rgba(255,246,159,1) 0%, rgba(255,212,74,1) 21%, rgba(255,245,145,1) 48%, rgba(255,209,60,1) 74%, rgba(255,246,159,1) 95%)`,
    height: "100%",
    width: "100%",
    transition: "opacity .5s",
  },
});

function DateIcon(props: SvgIconProps) {
  return (
    <SvgIcon {...props}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M13 0.5C13 0.223858 12.7761 0 12.5 0C12.2239 0 12 0.223858 12 0.5V2H4V0.5C4 0.223858 3.77614 0 3.5 0C3.22386 0 3 0.223858 3 0.5V2H2C0.895431 2 0 2.89543 0 4V5V6V14C0 15.1046 0.895431 16 2 16H14C15.1046 16 16 15.1046 16 14V6V5V4C16 2.89543 15.1046 2 14 2H13V0.5ZM3 3H2C1.44772 3 1 3.44772 1 4V5H15V4C15 3.44772 14.5523 3 14 3H13V3.5C13 3.77614 12.7761 4 12.5 4C12.2239 4 12 3.77614 12 3.5V3H4V3.5C4 3.77614 3.77614 4 3.5 4C3.22386 4 3 3.77614 3 3.5V3ZM15 6V14C15 14.5523 14.5523 15 14 15H2C1.44772 15 1 14.5523 1 14V6H15Z"
        fill="#676879"
      />
    </SvgIcon>
  );
}

function StarIcon(props: SvgIconProps) {
  return (
    <SvgIcon {...props}>
      <path
        d="M7.94505 0.542588C7.95633 0.51944 7.96763 0.510859 7.97356 0.507224C7.98095 0.502693 7.99 0.5 8 0.5C8.01 0.5 8.01905 0.502693 8.02644 0.507224C8.03237 0.510859 8.04367 0.51944 8.05495 0.542588L10.1359 4.8145L10.5854 4.59553L10.1359 4.8145C10.286 5.12256 10.5735 5.34747 10.9187 5.40499L15.4398 6.15829C15.4535 6.16057 15.4613 6.1652 15.4683 6.17172C15.477 6.17981 15.4875 6.1944 15.4941 6.21582C15.5079 6.26026 15.4972 6.29875 15.4719 6.32533L12.2369 9.71882C11.9996 9.96767 11.895 10.3138 11.9456 10.65L12.6589 15.3874C12.6665 15.4378 12.647 15.4701 12.6239 15.4876C12.612 15.4967 12.6021 15.4993 12.5969 15.4999C12.5939 15.5002 12.5869 15.5008 12.5723 15.493L8.49195 13.3184C8.18361 13.1541 7.81639 13.1541 7.50804 13.3184L3.42774 15.493C3.41314 15.5008 3.40609 15.5002 3.40313 15.4999C3.39789 15.4993 3.38795 15.4967 3.37605 15.4876C3.35301 15.4701 3.33353 15.4378 3.34111 15.3874L4.05436 10.65C4.10498 10.3138 4.00036 9.96767 3.76313 9.71881L0.528109 6.32533C0.502774 6.29875 0.49208 6.26026 0.505863 6.21582C0.512506 6.1944 0.52299 6.17981 0.531667 6.17172C0.53866 6.1652 0.546502 6.16057 0.560152 6.15829L5.08127 5.40499C5.42648 5.34747 5.71403 5.12256 5.8641 4.8145L7.94505 0.542588Z"
        stroke="#323338"
      />
    </SvgIcon>
  );
}

const ChallengeCard = (props: CardData) => {
  const [loading, setLoading] = useState<boolean>(false);
  const classes = useStyles();

  const handleChange = async (event: any) => {
    // props.completedByLoggedInUser = !props.completedByLoggedInUser
    setLoading(true);
    await props.updateCompletedUsers(props.itemId);
    setLoading(false);
    // setCompleted(props.completedByLoggedInUser)
  };

  let daysLeftText = "no time limit";
  if (props.date) {
    const dueDate = new Date(props.date);
    const today = new Date();
    const daysLeft = Math.round((dueDate.getTime() - today.getTime()) / (1000 * 3600 * 24));
    if (daysLeft < 0) {
      daysLeftText = "overdue";
    } else {
      daysLeftText = daysLeft.toString() + " days left";
    }
  }

  const thumbnailHeight = "120px";

  let thumbnailColor = "#805ADA";
  let thumbnailText = "😀";
  if (props.thumbnailText) {
    thumbnailText = props.thumbnailText;
  }

  if (props.thumbnailColor) {
    thumbnailColor = props.thumbnailColor;
  }

  if (props.completedByLoggedInUser) {
    thumbnailColor = `transparent`;
  }

  const thumbnail = (
    <div
      style={{
        height: thumbnailHeight,
        background: thumbnailColor,
        opacity: opacity,
        textAlign: "center",
        display: "flex",
        justifyContent: "center",
        alignContent: "center",
        flexDirection: "column",
        fontSize: 70,
      }}
    >
      {thumbnailText}
    </div>
  );

  let iconBanner = (
    <div
      style={{
        display: "flex",
        justifyContent: "space-between",
        flexDirection: "row",
        paddingBottom: "20px",
      }}
    >
      <div
        style={{
          display: "flex",
          justifyContent: "flex-start",
          alignItems: "center",
          flexDirection: "row",
        }}
      >
        <DateIcon viewBox="0 0 16 16" style={{ paddingRight: "10px" }} />
        <Typography variant="body2" component="span">
          {daysLeftText}
        </Typography>
      </div>
      <div
        style={{
          display: "flex",
          justifyContent: "flex-start",
          alignItems: "center",
          flexDirection: "row",
        }}
      >
        <StarIcon viewBox="0 0 16 16" fill="none" htmlColor="transparent" style={{ paddingRight: "10px" }} />
        <Typography variant="body2" component="span">
          {props.reward} stars
        </Typography>
      </div>
    </div>
  );

  if (props.completedByLoggedInUser) {
    const arr = [];
    let reward = 1;
    if (props.reward) {
      reward = props.reward;
    }
    for (let i = 0; i < reward; i++) {
      arr.push("jow");
    }
    iconBanner = (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "row",
          paddingBottom: "20px",
        }}
      >
        {arr.map((item, index) => (
          <div key={index} style={{ fontSize: "20px" }}>
            ⭐️
          </div>
        ))}
      </div>
    );
  }

  return (
    <Card className={`${classes.root}`}>
      <div className={`${props.completedByLoggedInUser && classes.overLay}`}>
        {thumbnail}{" "}
        <CardContent style={{ marginBottom: "20px" }}>
          {iconBanner}
          <Typography variant="subtitle1" component="p" gutterBottom>
            {props.name}
          </Typography>
          <Typography variant="body2" component="p" gutterBottom>
            {props.description}
          </Typography>
          <Typography variant="body2" component="p" color="primary">
            <Link href={props.linkUrl}>{props.linkText}</Link>
          </Typography>
        </CardContent>
        <CardActions
          style={{
            height: "60px",
          }}
        >
          <div
            style={{
              position: "absolute",
              bottom: "30px",
              width: "100%",
            }}
          >
            <Button
              disabled={props.completedByLoggedInUser || props.status === "closed"}
              variant="outlined"
              color="primary"
              size="medium"
              disableElevation={true}
              style={{
                margin: "0 auto",
                display: "block",
                fontSize: "13px",
                textTransform: "none",
                padding: "8px 16px 8px 16px",
              }}
              onClick={(event) => {
                handleChange(event);
              }}
            >
              <div style={{ display: "flex", alignItems: "center" }}>
                {props.status === "open" && <span>Complete challenge</span>}
                {props.status === "closed" && <span>Challenge closed</span>}
                {loading && <CircularProgress size={20} style={{ marginLeft: 10 }} />}
              </div>
            </Button>
          </div>
        </CardActions>
      </div>
    </Card>
  );
};

export default ChallengeCard;
