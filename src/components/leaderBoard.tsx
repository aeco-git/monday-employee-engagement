import React from "react";
import { LeaderBoardData } from "../types";
import Typography from "@material-ui/core/Typography";
import Avatar from "@material-ui/core/Avatar";
import Grid from "@material-ui/core/Grid";

type PropsType = {
  leaderBoardData: LeaderBoardData;
};

export default function LeaderBoard(props: PropsType) {
  return (
    <div>
      <div style={{ display: "flex", flexDirection: "column", alignItems: "center", margin: 20 }}>
        {props.leaderBoardData.map((item, index) => (
          <div
            key={index}
            style={{
              width: "100%",
              maxWidth: 500,
              borderColor: "#C5C7D0",
              borderStyle: "solid",
              height: 70,
              borderRadius: 8,
              borderWidth: 1,
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
              padding: 20,
              marginBottom: 10,
            }}
          >
            <Grid container direction="row" spacing={2}>
              <Grid item xs={3}>
                <div
                  style={{
                    width: 50,
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  {["🥇", "🥈", "🥉"].includes(item.rankSymbol) && (
                    <div style={{ fontSize: 40 }}>{item.rankSymbol}</div>
                  )}
                  {!["🥇", "🥈", "🥉"].includes(item.rankSymbol) && (
                    <div style={{ fontSize: 25 }}>{item.rankSymbol}</div>
                  )}
                </div>
              </Grid>
              <Grid item xs={6}>
                <div style={{ display: "flex", flexDirection: "row", alignItems: "center", height:"100%" }}>
                  <Avatar alt="Remy Sharp" src={item.photo_thumb} />
                  <Typography variant="subtitle1" component="span" style={{ paddingLeft: 10 }}>
                    {item.name}
                  </Typography>
                </div>
              </Grid>
              <Grid item xs={3}>
                <div style={{ fontSize: 20, height:"100%",display: "flex", flexDirection: "row", alignItems: "center", justifyContent:"flex-end", paddingRight:10 }}> {item.numberOfStars} ⭐️</div>
              </Grid>
            </Grid>
          </div>
        ))}
      </div>
    </div>
  );
}
