import React from "react";
import { makeStyles, Theme } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import { BoardData, LeaderBoardData } from "./../types";
import ChallengeBoard from "./challengeBoard";
import LeaderBoard from "./leaderBoard";
import PricesBoard from "./pricesBoard";

interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography component="span">{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: any) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
  },
}));

type PropsType = {
  boardState: BoardData;
  updateCompletedUsers: any;
  leaderBoardData: LeaderBoardData;
  pricesEnabled: boolean;
  user: number;
};

export default function CustomTabs(props: PropsType) {
  const classes = useStyles();
  const activeTab = 0;
  const [value, setValue] = React.useState(activeTab);

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" elevation={0}>
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="simple tabs example"
          centered
          style={{ boxShadow: "0 4px 4px -2px rgba(0, 0, 0, 0.2)" }}
          TabIndicatorProps={{
            style: {
              backgroundColor: "white",
            },
          }}
        >
          <Tab disableRipple label="Challenges" {...a11yProps(0)} />
          <Tab disableRipple label="Leader Board" {...a11yProps(1)} />
          {props.pricesEnabled && <Tab disableRipple label="Prices" {...a11yProps(2)} />}
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <ChallengeBoard boardState={props.boardState} updateCompletedUsers={props.updateCompletedUsers} />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <LeaderBoard leaderBoardData={props.leaderBoardData} />
      </TabPanel>
      {props.pricesEnabled && (
        <TabPanel value={value} index={2}>
          <PricesBoard
            boardState={props.boardState}
            updateCompletedUsers={props.updateCompletedUsers}
            user={props.user}
          />
        </TabPanel>
      )}
    </div>
  );
}
