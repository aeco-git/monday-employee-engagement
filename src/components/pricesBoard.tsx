import React from "react";
import { BoardData } from "../types";
import { getNumberOfStarsInWallet } from "./../helperFunctions/getNumberOfStars";
import PricesCard from "./pricesCard";
import Typography from "@material-ui/core/Typography";
import Countup from "react-countup";

type PropsType = {
  boardState: BoardData;
  updateCompletedUsers: any;
  user: number;
};

export default function PricesBoard(props: PropsType) {
  const starsToSpend = getNumberOfStarsInWallet(props.boardState, props.user)
  const walletComponent = (
    <div
      style={{
        height: 150,
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Typography variant="h3" component="h2">
        <div style={{ fontWeight: 500 }}>
          {" "}
          <Countup end={starsToSpend} duration={4} /> 🛒
        </div>
      </Typography>
      <Typography variant="body2" component="h1">
        <div style={{ fontWeight: 600 }}>Stars to spend</div>
      </Typography>
    </div>
  );

  return (
    <div>
      
      <div style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
        <div style={{ maxWidth: 1000, width: "100%" }}>{walletComponent}</div>
        
        {props.boardState.map((item, index) => {
          if (item.category === "Prices") {
            return (
              <div style={{ maxWidth: 1000, width:"100%" }} key={index}>
                <PricesCard
                  name={item.name}
                  description={item.description}
                  date={item.date}
                  status={item.status}
                  linkUrl={item.linkUrl}
                  linkText={item.linkText}
                  category={item.category}
                  reward={item.reward}
                  completed={item.completed}
                  itemId={item.itemId}
                  cost={item.cost}
                  thumbnailImage={item.thumbnailImage}
                  thumbnailText={item.thumbnailText}
                  thumbnailColor={item.thumbnailColor}
                  completedColumnId={item.completedColumnId}
                  completedByLoggedInUser={item.completedByLoggedInUser}
                  updateCompletedUsers={props.updateCompletedUsers}
                  starsToSpend={starsToSpend}
                />
              </div>
            );
          } else {
            return null;
          }
        })}
      </div>
    </div>
  );
}
