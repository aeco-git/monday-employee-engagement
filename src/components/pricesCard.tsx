import React, { useState } from "react";
import { PricesCardData } from "../types";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Link from "@material-ui/core/Link";
import Avatar from "@material-ui/core/Avatar";
import Divider from "@material-ui/core/Divider";
import CircularProgress from "@material-ui/core/CircularProgress";
import Grid from "@material-ui/core/Grid";


const PricesCard = (props: PricesCardData) => {
  const [loading, setLoading] = useState<boolean>(false);

  const handleChange = async (event: any) => {
    setLoading(true);
    await props.updateCompletedUsers(props.itemId);
    setLoading(false);
  };


  const buttonLook = (completedByLoggedInUser: boolean, starsToSpend: number, cost: number) => {
    const response = {
      buttonDisabled: false,
      buttonText: "",
    };

    if (completedByLoggedInUser) {
      response.buttonDisabled = true;
      response.buttonText = "Price requested";
    } else if (starsToSpend < cost) {
      response.buttonDisabled = true;
      response.buttonText = "Not enough stars";
    } else {
      response.buttonDisabled = false;
      response.buttonText = "Request price";
    }
    return response;
  };

  const { buttonDisabled, buttonText } = buttonLook(
    props.completedByLoggedInUser,
    props.starsToSpend,
    props.cost
  );


  let thumbnailImage =
    "https://www.usnews.com/dims4/USNEWS/fb8ba63/2147483647/thumbnail/970x647/quality/85/?url=http%3A%2F%2Fmedia.beam.usnews.com%2Fd3%2Fac%2F2fd358754dbfafeee6091c206204%2F200131-valentinesdaygift-stock.jpg";
  if (props.thumbnailImage) {
    thumbnailImage = props.thumbnailImage;
  }

  return (
    <div>
      <Grid container direction="row">
        <Grid item xs={3} style={{ display: "flex", alignItems: "center" }}>
          <Avatar
            alt="alt"
            src={thumbnailImage}
            variant="square"
            style={{ width: "100%", height: "auto", borderRadius: 16 }}
          />
        </Grid>
        <Grid item xs={6} style={{ display: "flex", alignItems: "center" }}>
          <div style={{ paddingLeft: 40 }}>
            <Typography variant="subtitle1" component="p">
              {props.name}
            </Typography>
            <Typography variant="body1" component="p">
              {props.description} <Link href={props.linkUrl}>{props.linkText}</Link>
            </Typography>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
                marginTop: 10,
              }}
            >
              {" "}
              <Button
                disabled={buttonDisabled}
                variant="outlined"
                color="primary"
                size="medium"
                disableElevation={true}
                style={{
                  display: "block",
                  fontSize: "13px",
                  textTransform: "none",
                  padding: "8px 16px 8px 16px",
                }}
                onClick={(event) => {
                  handleChange(event);
                }}
              >
                <div style={{ display: "flex", alignItems: "center" }}>
                  {buttonText}
                  {loading && <CircularProgress size={20} style={{ marginLeft: 10 }} />}
                </div>
              </Button>
            </div>
          </div>
        </Grid>
        <Grid item xs={3}>
          <div
            style={{
              fontSize: 25,
              display: "flex",
              height: "100%",
              alignItems: "center",
              justifyContent: "flex-end",
            }}
          >
            {props.cost} ⭐️
          </div>
        </Grid>
      </Grid>
      <Divider style={{ marginTop: 20, marginBottom: 20 }} />
    </div>
  );
};

export default PricesCard;
