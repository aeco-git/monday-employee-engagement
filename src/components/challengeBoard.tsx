import React from "react";
import { BoardData } from "../types";
import ChallengeCard from "./challengeCard";
import Grid from "@material-ui/core/Grid";

interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

type PropsType = {
  boardState: BoardData;
  updateCompletedUsers: any;
};

export default function ChallengeBoard(props: PropsType) {
  return (
    <div style={{ padding: 0 }}>
      {" "}
      <Grid container direction="row" spacing={2}>
        {props.boardState.map((item, index) => {
          if (item.category === "Challenges") {
            return (
              <Grid item xs={12} sm={6} md={4} lg={3} xl={2} key={index}>
                <ChallengeCard
                  name={item.name}
                  description={item.description}
                  date={item.date}
                  status={item.status}
                  linkUrl={item.linkUrl}
                  linkText={item.linkText}
                  category={item.category}
                  reward={item.reward}
                  completed={item.completed}
                  itemId={item.itemId}
                  thumbnailImage={item.thumbnailImage}
                  thumbnailText={item.thumbnailText}
                  thumbnailColor={item.thumbnailColor}
                  completedColumnId={item.completedColumnId}
                  completedByLoggedInUser={item.completedByLoggedInUser}
                  updateCompletedUsers={props.updateCompletedUsers}
                  cost={item.cost}
                />
              </Grid>
            );
          } else {
            return null;
          }
        })}
      </Grid>
    </div>
  );
}
