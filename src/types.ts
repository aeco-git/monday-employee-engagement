export type UnparsedBoardData = {
  name: string;
  column_values: [
    { value: string; title: "description" },
    { value: string; title: "date" },
    { value: string; title: "link" },
    { value: string; title: "category" },
    { value: string; title: "status" },
    { value: string; title: "reward" },
    { value: string; title: "completed" }
  ];
}[];

export type BoardData = {
  name: string;
  description?: string;
  date?: string;
  linkUrl?: string;
  linkText?: string;
  category: string;
  status: string;
  reward: number;
  completed?: number[];
  completedColumnId: string;
  itemId: string;
  cost: number;
  thumbnailImage?: string | null;
  thumbnailText?: string | null;
  thumbnailColor?: string | null;
  completedByLoggedInUser: boolean;
}[];

export type CardData = {
  name: string;
  description?: string;
  date?: string;
  linkUrl?: string;
  linkText?: string;
  category: string;
  status: string;
  reward: number;
  completed?: number[];
  completedColumnId: string;
  itemId: string;
  thumbnailImage?: string | null;
  thumbnailText?: string | null;
  thumbnailColor?: string | null;
  completedByLoggedInUser: boolean;
  updateCompletedUsers: any;
  cost: number;
};

export type PricesCardData = {
  name: string;
  description?: string;
  date?: string;
  linkUrl?: string;
  linkText?: string;
  category: string;
  status: string;
  reward: number;
  completed?: number[];
  completedColumnId: string;
  itemId: string;
  thumbnailImage?: string | null;
  thumbnailText?: string | null;
  thumbnailColor?: string | null;
  completedByLoggedInUser: boolean;
  updateCompletedUsers: any;
  cost: number;
  starsToSpend: number;
};

export type LeaderBoardData = {
  id: number;
  numberOfStars: number;
  photo_original: string;
  photo_thumb: string;
  name: string;
  rank: number;
  rankSymbol: string;
}[];
