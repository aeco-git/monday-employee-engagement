import React, { useState, useEffect } from "react";
import { getUser, getBoardData, getBoardUsers } from "./helperFunctions/graphQLRequests";
import { getNumberOfStars } from "./helperFunctions/getNumberOfStars";
import formatLeaderBoardData from "./helperFunctions/formatLeaderBoardData";
import { BoardData, LeaderBoardData } from "./types";
import mondaySdk from "monday-sdk-js";
// import dummyBoardState from "./localTesting/dummyBoardState.json";
// import dummyLeaderBoard from "./localTesting/dummyLeaderBoard.json";
import Typography from "@material-ui/core/Typography";
import Countup from "react-countup";
import CustomTabs from "./components/customTabs";
import { updateCompletedUsersOnItemQuery } from "./helperFunctions/graphQLRequests";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
const monday = mondaySdk();

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    backdrop: {
      zIndex: theme.zIndex.drawer + 1,
      color: "#fff",
    },
  })
);

const App = () => {
  const classes = useStyles();

  // console.log("[App.tsx]: render");
  const [boardState, setBoardState] = useState<BoardData>([]);
  const [user, setUser] = useState<number>(0);
  const [leaderBoardData, setLeaderBoardData] = useState<LeaderBoardData>([]);
  const [loading, setLoading] = useState<boolean>(true);
  // board Ids is always an array of only one number
  const [boardIds, setBoardIds] = useState<number[]>([0]);
  const [pricesEnabled, setPricesEnabled] = useState<boolean>(true);

  const updateCompletedUsers = async (itemId: string) => {
    // select the item of the boardState array that corresponds to the itemId
    const item = boardState.find(function (o) {
      return o.itemId === itemId;
    })!;
    await updateCompletedUsersOnItemQuery(
      item.itemId,
      user,
      item.completedColumnId,
      boardIds[0],
      item.completed
    );

    // update the board Data
    const boardData: BoardData = await getBoardData(boardIds);
    setBoardState(boardData);

    // update the leaderboard
    const boardUsers = await getBoardUsers(boardIds);
    const leaderBoardData = await formatLeaderBoardData(boardUsers, boardData);
    setLeaderBoardData(leaderBoardData);
  };

  useEffect(() => {
    // console.log("One time execution of useEffect => added event listener");

    monday.listen("settings", async (settingsListenResponse) => {
      // const settings = await monday.get("settings");
      // console.log("SETTINGS: ", settingsListenResponse);
      setPricesEnabled(settingsListenResponse["data"]["pricesEnabled"]);
    });

    monday.listen("context", async (contextListenResponse) => {
      // console.log("context listener executed");
      const boardIdsResponse = contextListenResponse.data.boardIds;
      setBoardIds(boardIdsResponse);
      const boardData: BoardData = await getBoardData(boardIdsResponse);
      const boardUsers = await getBoardUsers(boardIdsResponse);

      const leaderBoardData = await formatLeaderBoardData(boardUsers, boardData);
      // console.log("leaderbordData: ", JSON.stringify(leaderBoardData));

      setLoading(false);

      setLeaderBoardData(leaderBoardData);

      setBoardState(boardData);

      const userResponse = await getUser();
      setUser(userResponse);
    });
  }, []);

  const collectedComponent = (
    <div style={{ display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center" }}>
      <Typography variant="h3" component="h2">
        <div style={{ fontWeight: 500, color: "white" }}>
          {" "}
          <Countup end={getNumberOfStars(boardState, user)} duration={4} /> ⭐️
        </div>
      </Typography>
      <Typography variant="body2" component="h1">
        <div style={{ fontWeight: 600, color: "white" }}>Total stars collected</div>
      </Typography>
    </div>
  );

  return (
    <div className="App">
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "center",
          minHeight: "120px",
          background: "#0085FF",
          color: "white",
          boxShadow: "0.0px 6.0px 20.0px 0px rgba(0, 0, 0, 0.2)",
          paddingTop: 20,
        }}
      >
        {collectedComponent}
      </div>

      <CustomTabs
        boardState={boardState}
        updateCompletedUsers={updateCompletedUsers}
        leaderBoardData={leaderBoardData}
        pricesEnabled={pricesEnabled}
        user={user}
      />

      <Backdrop className={classes.backdrop} open={loading}>
        <CircularProgress color="inherit" />
      </Backdrop>
    </div>
  );
};

export default App;
