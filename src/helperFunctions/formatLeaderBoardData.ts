import {getNumberOfStars} from "./getNumberOfStars"

const formatLeaderBoardData = (boardUsers: any, boardData:any) => {
  let leaderBoardData = boardUsers;

  for (let user in boardUsers) {
    leaderBoardData[user]["numberOfStars"] = getNumberOfStars(boardData, boardUsers[user].id);
  }

  function compare(a: any, b: any) {
    if (a.numberOfStars < b.numberOfStars) {
      return 1;
    }
    if (a.numberOfStars > b.numberOfStars) {
      return -1;
    }
    return 0;
  }

  function setRankSymbol(rank: number) {
    if (rank === 1) {
      return "🥇";
    } else if (rank === 2) {
      return "🥈";
    } else if (rank === 3) {
      return "🥉";
    } else {
      return rank.toString();
    }
  }

  leaderBoardData = leaderBoardData.sort(compare);
  for (let i in leaderBoardData) {
    leaderBoardData[i]["rank"] = parseInt(i) + 1;
    leaderBoardData[i]["rankSymbol"] = setRankSymbol(parseInt(i) + 1);
  }
  return leaderBoardData;
};

export default formatLeaderBoardData