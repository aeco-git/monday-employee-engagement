import { BoardData } from "./../types";

const getNumberOfStars = (boardData: BoardData, user: number) => {
  let numberOfStars = 0;
  for (let item of boardData) {
    if (item.completed) {
      if (item.completed.includes(user)) {
        numberOfStars = numberOfStars + item.reward;
      }
    }
  }
  return numberOfStars;
};

const getNumberOfStarsInWallet = (boardData: BoardData, user:number) => {
  const numberOfStars = getNumberOfStars(boardData, user)
  let numberOfStarsSpend = 0
  for (let item of boardData) {
    if (item.completed) {
      if (item.completed.includes(user)) {
        numberOfStarsSpend = numberOfStarsSpend + item.cost;
      }
    }
  }  
  const wallet = numberOfStars - numberOfStarsSpend
  return wallet
}
export {getNumberOfStars, getNumberOfStarsInWallet};
