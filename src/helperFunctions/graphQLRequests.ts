import mondaySdk from "monday-sdk-js";
const monday = mondaySdk();

const getBoardUsers = async (boardIds: number[]) => {
  const response = await monday.api(
    `query ($boardIds: [Int]){boards (ids:$boardIds) {subscribers{id name photo_thumb photo_original} } }`,
    {
      variables: { boardIds: boardIds },
    }
  );

  const users = response.data.boards[0].subscribers

  return users;
};

const getBoardData = async (boardIds: number[]) => {
  const unparsedBoardData: any = (
    await monday.api(
      `query ($boardIds: [Int]) { boards (ids:$boardIds) {items {group{title} id name column_values {value title text id} } } }`,
      { variables: { boardIds: boardIds } }
    )
  ).data.boards[0].items;

  const user: number = (await monday.api(`query {me {id}}`)).data.me.id;

  // console.log("UNPARSED: ", unparsedBoardData);

  // TODO if the board does not contain the standard columns: return an error
  // TODO this error is captured in the App.tsx (or another component)
  // TODO when this error occurs a message is shown that the board is not formatted correctly
  // TODO alongside with a link to how to format the board

  const boardData = unparsedBoardData.map((item: any) => {
    const parsedColumnValues: any = {};
    parsedColumnValues["name"] = item.name;
    parsedColumnValues["itemId"] = item.id;
    parsedColumnValues["category"] = item.group.title;

    for (let column of item.column_values) {
      // special threatment for the thumbnail column
      if (column.title === "thumbnail image") {
        parsedColumnValues["thumbnailImage"] = column.text?.split(",")[0];
        delete parsedColumnValues["thumbnail image"];
      } else if (column.title === "status") {
        parsedColumnValues["status"] = column.text;
      } else if (column.title === "completed") {
        parsedColumnValues["completedColumnId"] = column.id;
        parsedColumnValues[column.title] = JSON.parse(column.value);
      } else {
        parsedColumnValues[column.title] = JSON.parse(column.value);
      }
    }

    // now that we have all column values in JSON it is time to extract the
    // usefull information
    parsedColumnValues["description"] = parsedColumnValues["description"]?.["text"];
    parsedColumnValues["linkUrl"] = parsedColumnValues["link"]?.["url"];
    parsedColumnValues["linkText"] = parsedColumnValues["link"]?.["text"];
    delete parsedColumnValues.link;
    parsedColumnValues["date"] = parsedColumnValues["date"]?.["date"];
    parsedColumnValues["reward"] = parsedColumnValues["reward"]?.["rating"] || 0;
    parsedColumnValues["cost"] = parseInt(parsedColumnValues["cost"]) || 0
    parsedColumnValues["thumbnailText"] = parsedColumnValues["thumbnail text"]?.["text"];
    delete parsedColumnValues["thumbnail text"];
    parsedColumnValues["thumbnailColor"] = parsedColumnValues["thumbnail color"]?.["color"]?.["hex"];
    delete parsedColumnValues["thumbnail color"];

    const persons = [];
    if (parsedColumnValues["completed"]) {
      if (parsedColumnValues["completed"]["personsAndTeams"]) {
        for (item of parsedColumnValues["completed"]["personsAndTeams"]) {
          if (item.kind === "person") {
            persons.push(item.id);
          }
        }
      }
    }

    parsedColumnValues["completed"] = persons;

    parsedColumnValues["completedByLoggedInUser"] = parsedColumnValues["completed"]
      ? parsedColumnValues["completed"].includes(user)
      : false;

    // console.log("PARSED COLUMN VALUES :", parsedColumnValues);

    return parsedColumnValues;
  });

  // console.log("updated board state: ", JSON.stringify(boardData));
  return boardData;
};

const getUser = async () => {
  return (await monday.api(`query {me {id}}`)).data.me.id;
};

// TODO continue on this:
const updateCompletedUsersOnItemQuery = async (
  item: string,
  user: number,
  userColumnId: string,
  boardId: number,
  completed: number[] | undefined
) => {
  let value;
  if (completed) {
    if (completed.toString() === "") {
      value = user.toString();
    } else if (completed.includes(user)) {
      // console.log("user already on this challenge");
      return null;
    } else {
      value = completed.join(",") + "," + user.toString();
    }
  } else {
    value = "";
  }

  // const response =
  await monday.api(
    `mutation($boardId: Int!, $itemId:Int!, $columnId:String!, $value:String!){change_simple_column_value (board_id:$boardId, item_id:$itemId, column_id:$columnId, value:$value){id}}`,
    // `mutation{change_simple_column_value (board_id: 841889328, item_id:849831514, column_id:"people9", value:"14119950"){id}}`,
    {
      variables: { boardId: boardId, itemId: parseInt(item), columnId: userColumnId, value: value },
    }
  );
  // console.log("Response from mutation: ", response);
};

export { getBoardData, getUser, updateCompletedUsersOnItemQuery, getBoardUsers };
