import { createMuiTheme } from "@material-ui/core/styles";

const theme = createMuiTheme({
  palette: {
    primary: {
      light: "#DFF0FF",
      main: "#0085FF",
      dark: "#0071D9",
      contrastText: "#fff",
    },
    secondary: {
      light: "#465c6d",
      main: "#183449",
      dark: "#102433",
      contrastText: "#fff",
    },
    background: {
      default: "#edf0f5"
    },
    text:{
      primary: "#323338",
      secondary: "#676879",
      hint: "#0085FF"
    }
  },
  typography: {
    h4: {
      paddingBottom: "1rem",
    },
    h5: {
      paddingBottom: "1rem",
    },
    subtitle1:{
      fontWeight:600,
      fontSize: "0.9rem"
    }
  },
});

export default theme;
