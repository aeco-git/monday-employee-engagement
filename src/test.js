dummyLeaderBoard = [
  {
    "id": 14119950,
    "name": "Ruben Aertgeerts",
    "photo_thumb": "https://files.monday.com/photos/14119950/thumb/14119950-Ruben_Aertgeerts_photo_2020_05_13_18_57_42.png?1589396262",
    "photo_original": "https://files.monday.com/photos/14119950/original/14119950-Ruben_Aertgeerts_photo_2020_05_13_18_57_42.png?1589396262",
    "numberOfStars": 5
  },
  {
    "id": 14129449,
    "name": "Thomas Aertgeerts",
    "photo_thumb": "https://files.monday.com/photos/14129449/thumb/14129449-Thomas_Aertgeerts_photo_2020_05_14_07_41_30.png?1589442090",
    "photo_original": "https://files.monday.com/photos/14129449/original/14129449-Thomas_Aertgeerts_photo_2020_05_14_07_41_30.png?1589442090",
    "numberOfStars": 6
  },
  {
    "id": 15331613,
    "name": "Arnout Aertgeerts",
    "photo_thumb": "https://files.monday.com/photos/15331613/thumb/15331613-data?1597586128",
    "photo_original": "https://files.monday.com/photos/15331613/original/15331613-data?1597586128",
    "numberOfStars": 3
  },
  {
    "id": 15331613,
    "name": "Arnout Aertgeerts",
    "photo_thumb": "https://files.monday.com/photos/15331613/thumb/15331613-data?1597586128",
    "photo_original": "https://files.monday.com/photos/15331613/original/15331613-data?1597586128",
    "numberOfStars": 50
  }
]



formatLeaderBoardData = (boardUsers) => {
  // for (let user in boardUsers) {
  //   boardUsers[user]["numberOfStars"] = getNumberOfStars(boardData, boardUsers[user].id);
  // }

  leaderBoardData = boardUsers

  function compare( a, b ) {
    if ( a.numberOfStars < b.numberOfStars ){
      return 1;
    }
    if ( a.numberOfStars > b.numberOfStars ){
      return -1;
    }
    return 0;
  }

  function setRankSymbol(rank){
    if (rank === 1){
      return "🥇"
    }
    else if (rank === 2){
      return "🥈"
    }
    else if (rank === 3){
      return "🥉"
    }
    else {
      return rank.toString()
    }
  }

  leaderBoardData = leaderBoardData.sort(compare)
  for (let i in leaderBoardData){
    leaderBoardData[i]["rank"] = parseInt(i)+1
    leaderBoardData[i]["rankSymbol"] = setRankSymbol(parseInt(i)+1)
  }
  return leaderBoardData
}
console.log("------------------------------------")
console.log(formatLeaderBoardData(dummyLeaderBoard));